/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isnan.telkom.recruitment.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 *
 * @author Yasmin
 */

@Document(collection = "account")
public class Account {
    @Id
    String id;
    String fullname;
    String username;
    String password;
    String email;
    String nama_file;
    String lokasi_file;
    String jurusan;
    
    


public Account() {
    }

    public Account(String id, String fullname, String username, String password, String email, String nama_file, String lokasi_file, String jurusan) {
        this.id = id;
        this.fullname = fullname;
        this.username = username;
        this.password = password;
        this. email = email;
        this.nama_file = nama_file;
        this.lokasi_file = lokasi_file;
        this.jurusan = jurusan;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getFullName() {
        return fullname;
    }

    public void setFullName(String fullname) {
        this.fullname = fullname;
    }
    
    public String getUserName() {
        return username;
    }

    public void setUserName(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getNamaFile() {
        return nama_file;
    }

    public void setNamaFile(String nama_file) {
        this.nama_file = nama_file;
    }
    
    public String getLokasiFile() {
        return lokasi_file;
    }

    public void setLokasiFile(String lokasi_file) {
        this.lokasi_file = lokasi_file;
    }
    
    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }
    
    
 



}
