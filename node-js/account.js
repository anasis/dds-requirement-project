function PersonSchema(data) {
    this.fullname = data.fullname;
    this.email = data.email;
    this.username = data.username;          
};

module.exports = PersonSchema;